import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';
import 'package:okshop_model/okshop_model.dart';
import 'enums.dart';

extension ExtensionInvoice on Invoice {
  SunmiSize get printMarkSize => isPrinted ? SunmiSize.xl : SunmiSize.xxl;
}
