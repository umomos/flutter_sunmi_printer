import 'package:barcode/barcode.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:okshop_model/okshop_model.dart';

class InvoicePage extends StatelessWidget {
  static const _TITLE_FONT_SIZE = 26.0;
  static const _DEF_FONT_SIZE = 20.0;
  final Invoice invoice;

  final _dm = Barcode.code39();
  final _qr = Barcode.qrCode(typeNumber: 6);

  InvoicePage({
    Key key,
    @required this.invoice,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _storeName(),
        _printMark(),
        _yearMonth(),
        _invoiceNumber(),
        _dateTime(),
        _randomAndTotal(),
        _sellerAndBuyer(),
        _divider(),
        _barCode(),
        _divider(),
        _doubleQrCode(),
      ],
    );
  }

  Widget _storeName() {
    return Text(
      // '企業人企業識別標章'
      invoice.storeName ?? '',
      style: const TextStyle(
        fontSize: _TITLE_FONT_SIZE,
        color: Colors.black,
      ),
      maxLines: 2,
      softWrap: true,
      textAlign: TextAlign.center,
    );
  }

  Widget _printMark() {
    return Text(
      invoice.displayPrintMark ?? '',
      maxLines: 1,
      softWrap: false,
      style: TextStyle(
        fontSize: this.invoice.isPrinted ? 30.0 : 36.0,
        color: Colors.black,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _yearMonth() {
    return Text(
      this.invoice.displayTwDateTime,
      maxLines: 1,
      softWrap: false,
      style: const TextStyle(
        fontSize: 40.0,
        color: Colors.black,
        fontWeight: FontWeight.w700,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _invoiceNumber() {
    return Text(
      // 'XX-12345678',
      this.invoice.displayInvoiceNumber ?? '',
      maxLines: 1,
      softWrap: false,
      style: const TextStyle(
        fontSize: 40.0,
        color: Colors.black,
        fontWeight: FontWeight.w700,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _dateTime() {
    return Row(
      children: [
        Expanded(
          child: Text(
            this.invoice.displayDateTime ?? '',
            maxLines: 1,
            softWrap: false,
            style: const TextStyle(
              fontSize: _DEF_FONT_SIZE,
              color: Colors.black,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Text(
          invoice.displayTaxType ?? '',
          maxLines: 1,
          softWrap: false,
          style: const TextStyle(
            fontSize: _DEF_FONT_SIZE,
            color: Colors.black,
          ),
          textAlign: TextAlign.left,
        ),
      ],
    );
  }

  Widget _randomAndTotal() {
    return Row(
      children: [
        Text(
          this.invoice.displayRandomNumber ?? '',
          maxLines: 1,
          softWrap: false,
          textAlign: TextAlign.left,
          style: const TextStyle(
            fontSize: _DEF_FONT_SIZE,
            color: Colors.black,
          ),
        ),
        Expanded(
          child: Text(
            this.invoice.displayTotalAmount ?? '',
            style: const TextStyle(
              fontSize: _DEF_FONT_SIZE,
              color: Colors.black,
            ),
            maxLines: 1,
            softWrap: false,
            textAlign: TextAlign.right,
          ),
        ),
      ],
    );
  }

  Widget _sellerAndBuyer() {
    return Row(
      children: [
        Expanded(
          child: Text(
            this.invoice.displaySeller ?? '',
            style: const TextStyle(
              fontSize: _DEF_FONT_SIZE,
              color: Colors.black,
            ),
            textAlign: TextAlign.left,
            maxLines: 1,
            softWrap: false,
          ),
        ),
        Expanded(
          child: Text(
            this.invoice.displayBuyer ?? '',
            style: const TextStyle(
              fontSize: _DEF_FONT_SIZE,
              color: Colors.black,
            ),
            textAlign: TextAlign.right,
            maxLines: 1,
            softWrap: false,
          ),
        ),
      ],
    );
  }

  Widget _barCode() {
    return SizedBox(
      // width: double.infinity,
      height: 60.0,
      child: SvgPicture.string(
        _dm.toSvg(
          this.invoice.barcode,
          height: 40.0,
          drawText: false,
          fontFamily: 'Libre Barcode 39',
        ),
      ),
    );
  }

  Widget _doubleQrCode() {
    return Row(
      children: [
        Expanded(
          child: SvgPicture.string(
            // this.leftQrSvgString,
            this._qr.toSvg(
                  this.invoice.leftQrString,
                  height: 150.0,
                  width: 150.0,
                ),
            height: 150.0,
          ),
        ),
        const SizedBox(
          width: 16.0,
        ),
        Expanded(
          child: SvgPicture.string(
            // this.rightQrSvgString,
            this._qr.toSvg(
                  this.invoice.rightQrString,
                  height: 150.0,
                  width: 150.0,
                ),
            height: 150.0,
          ),
        ),
      ],
    );
  }

  Widget _divider() {
    return const SizedBox(
      height: 8.0,
    );
  }
}
