import 'dart:convert';

import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';

import '../flutter_sunmi_printer.dart';

class SunmiUtil {
  ///
  ///
  ///
  static Future<void> printReportsRealtimeStatements(PrintStatements data) async {
    await SunmiPrinter.text(
      '${data.title}',
      styles: SunmiStyles(
        size: SunmiSize.def,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      '${data.subject}',
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '列印時間',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.date,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.hr();
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '商店名稱',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.storeName,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '人員',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.user,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await _printCenterReverse('銷售總額');
    // await printLeftRight('銷售淨額', '\$${data.orderAmount.currency}');
    // await printLeftRight('銷售稅金', '\$${data.orderTax.currency}');
    // await printLeftRight('銷售總額', '\$${data.orderTotal.currency}');
    await SunmiPrinter.row(
      textSize: SunmiSize.title,
      cols: [
        SunmiCol(
          text: '銷售淨額',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: '\$${data.orderAmount.decimalStyle}',
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.title,
      cols: [
        SunmiCol(
          text: '銷售稅金',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: '\$${data.orderTax.decimalStyle}',
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.title,
      cols: [
        SunmiCol(
          text: '銷售總額',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: '\$${data.orderTotal.decimalStyle}',
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    //
    await _printCenterReverse('各支付總額明細');
    if (data.payment != null && data.payment.isNotEmpty) {
      for (var i = 0; i < data.payment.length; i++) {
        await _printPayment(data.payment.elementAt(i));
        if (i + 1 < data.payment.length) {
          await SunmiPrinter.hr();
        }
      }
    } else {
      await printCenter('無資料');
    }
    //
    await _printCenterReverse('銷售訂單(不計算取消與退款)');
    // await printLeftRight('起始訂單編號', data.orderNumberStart);
    // await printLeftRight('結束訂單編號', data.orderNumberEnd);
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: '起始訂單編號',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.orderNumberStart ?? '',
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
      reverse: false,
    );
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: '結束訂單編號',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.orderNumberEnd ?? '',
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
      reverse: false,
    );
    await printLeftRight('門市訂單數', '${data.appOrderQuantity.toInt()}');
    await printLeftRight('門市收入', '\$${data.appOrderTotal.decimalStyle}');
    await printLeftRight('LINE訂單數', '${data.lineOrderQuantity.toInt()}');
    await printLeftRight('LINE收入', '\$${data.lineOrderTotal.decimalStyle}');
    await printLeftRight('現場減價', '\$${data.onSiteDiscount.decimalStyle}');
    await printLeftRight('額外費用收入', '\$${data.extraCharges.decimalStyle}');
    //
    // await _printCenterReverse('銷售退款');
    // for (var item in data.refundOrders) {
    //   await _printRefundOrder(item);
    //   await SunmiPrinter.hr();
    // }
    // await SunmiPrinter.row(
    //   textSize: SunmiSize.def,
    //   cols: [
    //     SunmiCol(
    //       text: '${data.refundQuantity.toInt()}',
    //       width: 6,
    //       align: SunmiAlign.right,
    //     ),
    //     SunmiCol(
    //       text: '\$${data.refundAmount.decimalStyle}',
    //       width: 6,
    //       align: SunmiAlign.right,
    //     ),
    //   ],
    // );
    // await SunmiPrinter.row(
    //   textSize: SunmiSize.def,
    //   cols: [
    //     SunmiCol(
    //       text: '加總數量',
    //       width: 6,
    //       align: SunmiAlign.right,
    //     ),
    //     SunmiCol(
    //       text: '加總金額',
    //       width: 6,
    //       align: SunmiAlign.right,
    //     ),
    //   ],
    // );
    // await hr2();
    // await _printCenterReverse('發票');
    // await printLeftRight('起始發票號', data.invoiceNumberStart);
    // await printLeftRight('結束發票號', data.invoiceNumberEnd);
    // await printLeftRight('發票張數', '${data.invoiceQuantity.toInt()}');
    // await printLeftRight('開立發票總金額', '\$${data.invoiceAmount.decimalStyle}');
    // await printLeftRight('作廢發票張數', '${data.invoiceVoidQuantity.toInt()}');
    // await printLeftRight('作廢發票總金額', '\$${data.invoiceVoidAmount.decimalStyle}');
    // await SunmiPrinter.hr();
    // data.invoiceVoidNumber ??= <String>[];
    // await printLeftRight('作廢發票', data.invoiceVoidNumber.isEmpty ? '無資料' : '');
    // for (var item in data.invoiceVoidNumber) {
    //   await SunmiPrinter.text(
    //     item,
    //     styles: SunmiStyles(
    //       align: SunmiAlign.right,
    //       size: SunmiSize.def,
    //     ),
    //   );
    // }
    await SunmiPrinter.print3Line();
    await SunmiPrinter.cutPaper();
  }

  ///
  /// 當日營收統計
  ///
  static Future<void> printReportsStatements(PrintStatements data) async {
    await SunmiPrinter.text(
      '${data.title}',
      styles: SunmiStyles(
        size: SunmiSize.def,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      '${data.subject}',
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '列印時間',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.date,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.hr();
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '商店名稱',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.storeName,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '人員',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.user,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await _printCenterReverse('銷售總額');
    // await printLeftRight('銷售淨額', '\$${data.orderAmount.currency}');
    // await printLeftRight('銷售稅金', '\$${data.orderTax.currency}');
    // await printLeftRight('銷售總額', '\$${data.orderTotal.currency}');
    await SunmiPrinter.row(
      textSize: SunmiSize.title,
      cols: [
        SunmiCol(
          text: '銷售淨額',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: '\$${data.orderAmount.decimalStyle}',
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.title,
      cols: [
        SunmiCol(
          text: '銷售稅金',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: '\$${data.orderTax.decimalStyle}',
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.title,
      cols: [
        SunmiCol(
          text: '銷售總額',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: '\$${data.orderTotal.decimalStyle}',
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    //
    await _printCenterReverse('各支付總額明細');
    if (data.payment != null && data.payment.isNotEmpty) {
      for (var i = 0; i < data.payment.length; i++) {
        await _printPayment(data.payment.elementAt(i));
        if (i + 1 < data.payment.length) {
          await SunmiPrinter.hr();
        }
      }
    } else {
      await printCenter('無資料');
    }
    //
    await _printCenterReverse('銷售訂單(不計算取消與退款)');
    // await printLeftRight('起始訂單編號', data.orderNumberStart);
    // await printLeftRight('結束訂單編號', data.orderNumberEnd);
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: '起始訂單編號',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.orderNumberStart ?? '',
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
      reverse: false,
    );
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: '結束訂單編號',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.orderNumberEnd ?? '',
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
      reverse: false,
    );
    await printLeftRight('門市訂單數', '${data.appOrderQuantity.toInt()}');
    await printLeftRight('門市收入', '\$${data.appOrderTotal.decimalStyle}');
    await printLeftRight('LINE訂單數', '${data.lineOrderQuantity.toInt()}');
    await printLeftRight('LINE收入', '\$${data.lineOrderTotal.decimalStyle}');
    await printLeftRight('現場減價', '\$${data.onSiteDiscount.decimalStyle}');
    await printLeftRight('額外費用收入', '\$${data.extraCharges.decimalStyle}');
    //
    await _printCenterReverse('銷售退款');
    for (var item in data.refundOrders) {
      await _printRefundOrder(item);
      await SunmiPrinter.hr();
    }
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '${data.refundQuantity.toInt()}',
          width: 6,
          align: SunmiAlign.right,
        ),
        SunmiCol(
          text: '\$${data.refundAmount.decimalStyle}',
          width: 6,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '加總數量',
          width: 6,
          align: SunmiAlign.right,
        ),
        SunmiCol(
          text: '加總金額',
          width: 6,
          align: SunmiAlign.right,
        ),
      ],
    );
    await hr2();
    await _printCenterReverse('發票');
    await printLeftRight('起始發票號', data.invoiceNumberStart);
    await printLeftRight('結束發票號', data.invoiceNumberEnd);
    await printLeftRight('發票張數', '${data.invoiceQuantity.toInt()}');
    await printLeftRight('開立發票總金額', '\$${data.invoiceAmount.decimalStyle}');
    await printLeftRight('作廢發票張數', '${data.invoiceVoidQuantity.toInt()}');
    await printLeftRight('作廢發票總金額', '\$${data.invoiceVoidAmount.decimalStyle}');
    await SunmiPrinter.hr();
    data.invoiceVoidNumber ??= <String>[];
    await printLeftRight('作廢發票', data.invoiceVoidNumber.isEmpty ? '無資料' : '');
    for (var item in data.invoiceVoidNumber) {
      await SunmiPrinter.text(
        item,
        styles: SunmiStyles(
          align: SunmiAlign.right,
          size: SunmiSize.def,
        ),
      );
    }
    await SunmiPrinter.print3Line();
    await SunmiPrinter.cutPaper();
  }

  static Future<void> _printRefundOrder(RefundOrder data) async {
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: '訂單編號',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.orderNumber,
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
      reverse: false,
    );
    await printLeftRight('發票號碼', data.invoiceNumber);
    await printLeftRight('訂單金額', '\$${data.orderTotal.decimalStyle}');
    await printLeftRight('原支付方式', data.paymentName);
    await printLeftRight('退款者', data.memberName);
    await printLeftRight('處理者', data.refundStoreAccountName);
  }

  static Future<void> _printPayment(Payment data) async {
    await printLeftRight(
        '${data.channelPayMethodName}收入', '\$${data.income.decimalStyle}');
    await printLeftRight(
        '${data.channelPayMethodName}支出', '\$${data.expenses.decimalStyle}');
  }

  ///
  /// 當日商品銷售統計
  ///
  static Future<void> printReportsSales(PrintSales data) async {
    await SunmiPrinter.text(
      '${data.title}',
      styles: SunmiStyles(
        size: SunmiSize.def,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      '${data.subject}',
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '列印時間',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.date,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.hr();
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '商店名稱',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.storeName,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: '人員',
          width: 4,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: data.user,
          width: 8,
          align: SunmiAlign.right,
        ),
      ],
    );
    await _printCenterReverse('門市銷售');
    data.app ??= <App>[];
    if (data.app.isNotEmpty) {
      await _printSales(data.app);
    } else {
      await printCenter('無資料');
    }
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      '\$${data.appTotal.decimalStyle}',
      styles: SunmiStyles.right(size: SunmiSize.lg),
    );
    await SunmiPrinter.text(
      '一般銷售小計',
      styles: SunmiStyles.right(),
    );
    await SunmiPrinter.hr();
    await printLeftRightArgs(
      PrintArgs(
        '${data.app.length}',
        align: SunmiAlign.right,
        size: SunmiSize.title,
      ),
      PrintArgs(
        '${data.appQuantity.toInt()}',
        size: SunmiSize.title,
      ),
    );
    await printLeftRightArgs(
      PrintArgs('合計品項', align: SunmiAlign.right),
      PrintArgs('合計銷售數量'),
    );
    await hr2();
    await _printCenterReverse('LINE銷售');
    data.online ??= <App>[];
    if (data.online.isNotEmpty) {
      await _printSales(data.online);
    } else {
      await printCenter('無資料');
    }
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      '\$${data.onlineTotal.decimalStyle}',
      styles: SunmiStyles.right(size: SunmiSize.lg),
    );
    await SunmiPrinter.text(
      '一般銷售小計',
      styles: SunmiStyles.right(),
    );
    await SunmiPrinter.hr();
    await printLeftRightArgs(
      PrintArgs('${data.online.length}', align: SunmiAlign.right),
      PrintArgs('${data.onlineQuantity.toInt()}'),
    );
    await printLeftRightArgs(
      PrintArgs('合計品項', align: SunmiAlign.right),
      PrintArgs('合計銷售數量'),
    );
    await hr2();
    await SunmiPrinter.print3Line();
    await SunmiPrinter.cutPaper();
  }

  ///
  ///
  ///
  static Future<void> hr2() async {
    // final hr = List.filled(15, '-').join();
    final hr = List.filled(14, '─').join();
    await printLeftRightArgs(PrintArgs(hr), PrintArgs(hr));
  }

  ///
  /// 列印所有品項
  ///
  static Future<void> _printSales(Iterable<App> data) async {
    for (var item in data) {
      await _printSalesItem(item);
    }
  }

  ///
  /// 列印單一品項
  ///
  static Future<void> _printSalesItem(App data) async {
    await printText(data.productTitle);
    // await printLeftRight(
    //     '  \$${data.price.currency} X ${data.quantity.toInt()}',
    //     '\$${data.total.currency}');
    await printLeftRightArgs(
      PrintArgs('  \$${data.price.decimalStyle} X ${data.quantity.toInt()}'),
      PrintArgs('\$${data.total.decimalStyle}'),
    );
  }

  ///
  /// 列印消費明細
  ///
  static Future<void> printReceiptLite(Receipt receipt) async {
    await SunmiPrinter.text(
      '消費明細',
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    await printText('列印時間：${receipt.displayPrintAt} ${receipt.userName}');
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '內用 168',
          text: '${receipt.displayType} ${receipt.orderNumberSimply}',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '訂單退貨、退款',
          // text: receipt.displayStatus,
          // 更正為顯示桌號，訂單狀態於下方已有顯示
          text: receipt.seat ?? '',
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize(30),
      reverse: true,
    );
    // await SunmiPrinter.hr();
    final orderNumber = receipt.orderNumber ?? '';
    await printText('訂單編號：$orderNumber');
    if (receipt.displayCheckoutAt != null &&
        receipt.displayCheckoutAt.isNotEmpty) {
      await printText('結帳時間：${receipt.displayCheckoutAt}');
    }
    if (receipt.invoiceNumber != null && receipt.invoiceNumber.isNotEmpty) {
      await printText(
          '發票號碼：${receipt.invoiceNumber} ${receipt.displayInvoiceStatus}');
    }
    if (receipt.vatNumber != null && receipt.vatNumber.isNotEmpty) {
      await printText('統一編號：${receipt.vatNumber}');
    }
    if (receipt.carrierId != null && receipt.carrierId.isNotEmpty) {
      await printText('手機條碼載具：${receipt.carrierId}');
    }
    if (receipt.npoBan != null && receipt.npoBan.isNotEmpty) {
      await printText('愛心碼：${receipt.npoBan}');
    }
    if (receipt.paymentName != null && receipt.paymentName.isNotEmpty) {
      await printText('支付方式：${receipt.paymentName}');
    }
    await SunmiPrinter.hr();
    await printText(
        '合計 ${receipt.billCount}訂單，${receipt.normalItemsCount}項，${receipt.displaySubtotal}元');
    await SunmiPrinter.hr();
    if (receipt.storeType.isDinner) {
      // TODO: 外送也可能有運費
      await SunmiPrinter.row(
        cols: [
          SunmiCol(
            text: '商品小計:${receipt.displaySubtotal}',
            width: 7,
            align: SunmiAlign.left,
          ),
          SunmiCol(
            text: '服務費:${receipt.displayServiceFee}',
            width: 5,
            align: SunmiAlign.right,
          ),
        ],
        textSize: SunmiSize.def,
        reverse: false,
      );
    }
    if (receipt.storeType.isRetail) {
      await SunmiPrinter.row(
        cols: [
          SunmiCol(
            text: '商品小計:${receipt.displaySubtotal}',
            width: 7,
            align: SunmiAlign.left,
          ),
          SunmiCol(
            text: '運費:${receipt.displayDeliveryFee}',
            width: 5,
            align: SunmiAlign.right,
          ),
        ],
        textSize: SunmiSize.def,
        reverse: false,
      );
    }
    await printLeftRight(
        '現場減價:${receipt.displayDiscount}', '額外費用:${receipt.displayOtherFee}');
    // 積點折抵
    if (receipt.redeemMemberPoints is num && receipt.redeemMemberPoints != 0) {
      final pointDiscount = receipt.redeemMemberPoints.abs() * (-1);
      await printText('積點折抵:${pointDiscount.decimalStyle}');
    }
    if (receipt.storeType.isRetail) {
      // await printText('${receipt.paymentName}：${receipt.displayPaymentFee}');
      await printText('金流手續費:${receipt.displayPaymentFee}');
    }
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      // '商品總價：620元',
      '商品總價：${receipt.displayTotal}元',
      styles: SunmiStyles(
        size: SunmiSize.title,
        bold: true,
      ),
    );
    await printLeftRight(
        '實收:${receipt.displayPaid}', '找零:${receipt.displayChange}');
    await SunmiPrinter.print3Line();
    await SunmiPrinter.cutPaper();
  }

  ///
  /// 列印商品明細 (前結用)
  ///
  static Future<void> printReceiptItems(Receipt receipt) async {
    await SunmiPrinter.text(
      // '餐點/商品明細',
      '${receipt.storeType.receipt}明細',
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    // await SunmiPrinter.hr();
    // await printText('列印時間：${receipt.displayPrintAt} ${receipt.userName}');
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '內用 168',
          text: '${receipt.displayType} ${receipt.orderNumberSimply}',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '訂單退貨、退款',
          // text: receipt.displayStatus,
          // 更正為顯示桌號，訂單狀態於下方已有顯示
          text: receipt.seat ?? '',
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize(30),
      reverse: true,
    );
    await printText('訂單編號：${receipt.orderNumber ?? ''}');
    await printText('訂單時間：${receipt.displayCreatedAt}');
    await printText('訂單狀態：${receipt.displayStatus}');
    // await printText('${receipt.displayCategory}方式：${receipt.displayType}');
    await printText('付款狀態：${receipt.displayPaymentStatus}');
    // 餐飲 - 列出用餐時間
    if (receipt.storeType.isDinner) {
      await printText('${receipt.displayCategory}時間：${receipt.displayMealAt}');
    }

    if (receipt.buyerName != null && receipt.buyerName.isNotEmpty) {
      await printText('${receipt.target}姓名：${receipt.buyerName ?? ''}');
    }
    // await printText('顧客電話：${receipt.buyerPhone ?? ''}');
    if (receipt.buyerPhone != null && receipt.buyerPhone.isNotEmpty) {
      await printText('${receipt.target}電話：${receipt.buyerPhone ?? ''}');
    }
    if (receipt.buyerAddress != null &&
        receipt.buyerAddress.isNotEmpty &&
        receipt.needAddress) {
      await printText('${receipt.target}地址：${receipt.buyerAddress ?? ''}');
    }
    if (receipt.displayBuyerMemo != null &&
        receipt.displayBuyerMemo.isNotEmpty) {
      await printText('顧客備註：${receipt.displayBuyerMemo ?? ''}');
    }
    await SunmiPrinter.hr();
    receipt.items ??= [];
    for (Item item in receipt.items) {
      // await printText('黃金泡菜(3入裝)');
      await printText(item.itemName);
      // await printText('•2倍泡菜 •不辣');
      if (item.comment?.isNotEmpty ?? false) {
        await printText(item.comment);
      }
      // await printLeftRight('  \$1,080 x 1', '1,080元');
      await printLeftRight(item.leftString, '${item.displayTotal}元');
    }
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      // '小計：2項 金額：1,280元',
      '小計：${receipt.normalItemsCount}項 金額：${receipt.displayTotal}元',
      styles: SunmiStyles(
        align: SunmiAlign.right,
        size: SunmiSize.def,
      ),
    );
    // if (receipt.servicePercentage != null && receipt.servicePercentage > 0) {
    //   await SunmiPrinter.text(
    //     '(本次消費服務費${receipt.servicePercentage.round()}%另計)',
    //     styles: SunmiStyles(
    //       align: SunmiAlign.center,
    //       size: SunmiSize.def,
    //     ),
    //   );
    // }
    if (receipt.orderId != null) {
      final jsonObject = {
        'type': 'order',
        'order_id': receipt.orderId,
      };
      await SunmiPrinter.emptyLines(1);
      await SunmiPrinter.setAlign(SunmiAlign.center);
      await SunmiPrinter.printQrCode(
        data: jsonEncode(jsonObject),
        modulesize: 6,
        errorlevel: ErrorLevel.L,
      );
    }
    await SunmiPrinter.print3Line();
    await SunmiPrinter.cutPaper();
  }

  ///
  /// 列印消費明細 (預覽時使用，最後會棄用)
  /// printReceiptLite + printReceiptItems
  ///
  static Future<void> printReceipt(Receipt receipt) async {
    await SunmiPrinter.text(
      // '洄瀾禮好有限公司',
      receipt.storeName,
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    // await printText('列印時間：03-17 17:30 小明');
    await printText('列印時間：${receipt.displayPrintAt} ${receipt.userName}');
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '內用 168',
          text: '${receipt.displayType} ${receipt.orderNumberSimply}',
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '訂單退貨、退款',
          // text: receipt.displayStatus,
          // 更正為顯示桌號，訂單狀態於下方已有顯示
          text: receipt.seat ?? '',
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize(30),
      reverse: true,
    );
    await printHeader('消費明細');
    // await printText('主訂單編號：line2021098765');
    await printText('訂單編號：${receipt.orderNumber}');
    // await printText('結帳時間：03-17 18:20');
    await printText('結帳時間：${receipt.displayCheckoutAt}');
    // await printText('發票號碼：QM-55560938已作廢');
    if (receipt.invoiceNumber != null && receipt.invoiceNumber.isNotEmpty) {
      await printText(
          '發票號碼：${receipt.invoiceNumber} ${receipt.displayInvoiceStatus}');
    }
    // await printText('統一編號：12344321');
    if (receipt.vatNumber != null && receipt.vatNumber.isNotEmpty) {
      await printText('統一編號：${receipt.vatNumber}');
    }
    if (receipt.carrierId != null && receipt.carrierId.isNotEmpty) {
      await printText('手機條碼載具：${receipt.carrierId}');
    }
    if (receipt.npoBan != null && receipt.npoBan.isNotEmpty) {
      await printText('愛心碼：${receipt.npoBan}');
    }
    // await printText('支付方式：-');
    // await printText('支付方式：${receipt.displayPaymentMethodId}');
    await printText('支付方式：${receipt.paymentName ?? ''}');
    await SunmiPrinter.hr();
    // await printText('合計 1訂單，3項，566元');
    await printText(
        '合計 ${receipt.billCount}訂單，${receipt.normalItemsCount}項，${receipt.displaySubtotal}元');
    await SunmiPrinter.hr();
    // await printLeftRight('商品小計:566', '運費：0');
    if (receipt.storeType.isDinner) {
      // TODO: 外送也可能有運費
      await printLeftRight('商品小計:${receipt.displaySubtotal}',
          '服務費:${receipt.displayServiceFee}');
    }
    if (receipt.storeType.isRetail) {
      await printLeftRight('商品小計:${receipt.displaySubtotal}',
          '運費:${receipt.displayDeliveryFee}');
    }
    // await printLeftRight('現場減價：0', '額外費用：0');
    await printLeftRight(
        '現場減價:${receipt.displayDiscount}', '額外費用:${receipt.displayOtherFee}');
    // 積點折抵
    if (receipt.redeemMemberPoints is num && receipt.redeemMemberPoints != 0) {
      final pointDiscount = receipt.redeemMemberPoints.abs() * (-1);
      await printText('積點折抵:${pointDiscount.decimalStyle}');
    }
    // 金流手續費
    if (receipt.storeType.isRetail) {
      // await printText('${receipt.paymentName}：${receipt.displayPaymentFee}');
      await printText('金流手續費:${receipt.displayPaymentFee}');
    }
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      // '商品總價：620元',
      '商品總價：${receipt.displayTotal}元',
      styles: SunmiStyles(
        size: SunmiSize.title,
        bold: true,
      ),
    );
    // await printLeftRight('實收：620', '找零：0');
    await printLeftRight(
        '實收：${receipt.displayPaid}', '找零：${receipt.displayChange}');
    await SunmiPrinter.hr();
    // await printText('訂單編號：line2021098765');
    await printText('訂單編號：${receipt.orderNumber ?? ''}');
    // await printText('訂單時間：2021-03-17 15:20');
    await printText('訂單時間：${receipt.displayCreatedAt}');
    // 訂單狀態顯示在這
    await printText('訂單狀態：${receipt.displayStatus}');
    // await printText('取貨方式：自取');
    await printText('${receipt.displayCategory}方式：${receipt.displayType}');
    // await printText('付款方式：轉帳匯款');
    // await printText('付款方式：${receipt.paymentName ?? ''}');
    // await printText('付款狀態：未付款');
    await printText('付款狀態：${receipt.displayPaymentStatus}');
    // await printText('取貨時間：2021-06-23 15:30');
    if (receipt.storeType.isDinner) {
      // 餐飲 - 列出用餐時間
      await printText('${receipt.displayCategory}時間：${receipt.displayMealAt}');
    }
    if (receipt.type.orderType.storeType.isRetail) {
      // 零售 - 拿掉取貨時間
      // await printText('${receipt.displayCategory}時間：${receipt.displayMealAt}');
    }
    // await printText('取貨人姓名：釋金城');
    await printText('${receipt.target}姓名：${receipt.buyerName ?? ''}');
    // await printText('顧客電話：${receipt.buyerPhone ?? ''}');
    await printText('${receipt.target}電話：${receipt.buyerPhone ?? ''}');
    // await printText('顧客地址：${receipt.buyerAddress ?? ''}');
    if (receipt.needAddress) {
      await printText('${receipt.target}地址：${receipt.buyerAddress ?? ''}');
    }
    // await printText('顧客備註：不需要塑膠袋裝。');
    await printText('顧客備註：${receipt.displayBuyerMemo ?? ''}');
    // await printText('店家備註：-');
    // 取消列印店家備註，不能讓消費者看到壞話
    // await printText('店家備註：${receipt.displaySellerMemo ?? ''}');
    // await SunmiPrinter.hr();
    await printHeader('商品明細');
    // await SunmiPrinter.hr();
    for (Item item in receipt.items) {
      // await printText('黃金泡菜(3入裝)');
      await printText(item.itemName);
      // await printText('•2倍泡菜 •不辣');
      if (item.comment?.isNotEmpty ?? false) {
        await printText(item.comment);
      }
      // await printLeftRight('  \$1,080 x 1', '1,080元');
      await printLeftRight(item.leftString, '${item.displayTotal}元');
    }
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      // '小計：2項 金額：1,280元',
      '小計：${receipt.normalItemsCount}項 金額：${receipt.displayTotal}元',
      styles: SunmiStyles(
        align: SunmiAlign.right,
        size: SunmiSize.def,
      ),
    );
    if (receipt.orderId != null) {
      final jsonObject = {
        'type': 'order',
        'order_id': receipt.orderId,
      };
      await SunmiPrinter.emptyLines(1);
      await SunmiPrinter.setAlign(SunmiAlign.center);
      await SunmiPrinter.printQrCode(
        data: jsonEncode(jsonObject),
        modulesize: 6,
        errorlevel: ErrorLevel.L,
      );
    }
    await SunmiPrinter.print3Line();
    await SunmiPrinter.cutPaper();
  }

  ///
  /// 列印折讓單
  ///
  static Future<void> printDiscount(Invoice invoice) async {
    await SunmiPrinter.text(
      // '洄瀾禮好有限公司',
      invoice.storeName,
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    await printCenter('營業人銷貨退回、進貨退出或');
    await printCenter('折讓證明單');
    // await printCenter('2017-12-01');
    await printCenter(invoice.displayDate);
    // await printText('賣方統編：83079973');
    await printText('賣方統編：${invoice.seller}');
    // await printText('賣方名稱：洄瀾禮好有限公司');
    await printText('賣方名稱：${invoice.storeName}');
    // await printText('發票開立日期：2017-12-01');
    await printText('發票開立日期：${invoice.displayDate}');
    // await printText('AB-00000010');
    await printText('${invoice.displayInvoiceNumber}');
    // await printText('買方統編：12656354');
    await printText('買方統編：${invoice.buyer}');
    // await printText('買方名稱：金財通商務科技服務股份有限公司');
    await printText('買方名稱：${invoice.buyerName}');
    await SunmiPrinter.emptyLines(1);
    await printHeader('銷貨明細表');
    // await printText('商品一批 \$1,000 * 1');
    // await printText('1,000 TX');
    for (var item in invoice.items) {
      await printText(item.itemName);
      await printLeftRight(item.leftString, item.rightString);
    }
    // await printLeftRight('合計：', '1項');
    await printLeftRight('合計：', invoice.displayItemsCount);
    // await printLeftRight('銷售額：', '952元');
    await printLeftRight('銷售額：', invoice.displayItemsSalesAmount);
    // await printLeftRight('稅額：', '48元');
    await printLeftRight('稅額：', invoice.displayItemsTaxAmount);
    // await printLeftRight('總計：', '1,000元');
    await printLeftRight('總計：', invoice.displayItemsTotalAmount);
    await SunmiPrinter.hr();
    await printText('簽收人：');
    await SunmiPrinter.print3Line();
    await SunmiPrinter.cutPaper();
  }

  ///
  /// 列印發票
  ///
  static Future<void> printInvoice(Invoice invoice) async {
    await SunmiPrinter.text(
      // '全家就是你家',
      invoice.storeName ?? '',
      styles: SunmiStyles(
        size: SunmiSize.xl,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      // '電子發票證明聯',
      invoice.displayPrintMark,
      styles: SunmiStyles(
        size: invoice.printMarkSize,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      // '110年05-06月',
      invoice.displayTwDateTime,
      styles: SunmiStyles(
        bold: true,
        size: SunmiSize(54),
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      // 'NS-00000334',
      invoice.displayInvoiceNumber,
      styles: SunmiStyles(
        bold: true,
        size: SunmiSize(54),
        align: SunmiAlign.center,
      ),
    );
    // await SunmiPrinter.setAlign(SunmiAlign.left);
    // SunmiPrinter.text(
    //   //'2021/05/12 18:42:40',
    //   invoice.displayDateTime,
    //   styles: SunmiStyles(
    //     bold: false,
    //     size: SunmiSize(30),
    //     align: SunmiAlign.left,
    //   ),
    // );
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: invoice.displayDateTime,
          width: 9,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: invoice.displayTaxType,
          width: 3,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize(30),
    );
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '隨機碼7311',
          text: invoice.displayRandomNumber,
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '總計9,993,782',
          text: invoice.displayTotalAmount,
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize(30),
    );
    await printLeftRight(
      invoice.displaySeller,
      invoice.displayBuyer,
      textSize: SunmiSize(30),
    );
    await SunmiPrinter.setAlign(SunmiAlign.center);
    await SunmiPrinter.printBarCode(
      // data: '10906KF427741079473',
      data: invoice.barcode,
      symbology: BarCodeSymbology.CODE39,
      height: 70,
      width: 1,
      textposition: TextPosition.NONE,
    );
    await SunmiPrinter.printDoubleQrCode(
      // code1:
      //     'AB112233441020523999900000145000001540000000001234567ydXZt4LAN1UHN/j1juVcRA==:**********:3:3:2:5Lm+6Zu75rGg'
      //         .padRight(128, ' '),
      // code2:
      //     '**OjE6MTA1OuWPo+e9qToxOjIxMDrniZvlpbY6MToyNQ=='.padRight(128, ' '),
      code1: invoice.leftQrString,
      code2: invoice.rightQrString,
      modulesize: 210,
      errorlevel: ErrorLevel.L,
    );
    await _printItems(invoice);
    await SunmiPrinter.emptyLines(3);
    await SunmiPrinter.cutPaper();
  }

  static Future<void> _printItems(Invoice invoice) async {
    await printHeader('銷貨明細表');
    for (final item in invoice.items) {
      await printText(item.itemName);
      if (item.comment?.isNotEmpty ?? false) {
        await printText('  ${item.comment}');
      }
      await printLeftRight(item.leftString, item.rightString);
    }
    // await printLeftRight('合計', '1項');
    await printLeftRight('合計', invoice.displayItemsCount);
    // await printLeftRight('銷售額', '952元');
    if (invoice.showTax) {
      await printLeftRight('應稅銷售額', invoice.displayItemsTXSalesAmount);
      await printLeftRight('免稅銷售額', invoice.displayItemsFreeSalesAmount);
    } else {
      await printLeftRight('銷售額', invoice.displayItemsSalesAmount);
    }
    if (invoice.showTax) {
      // await printLeftRight('稅額', '48元');
      await printLeftRight('稅額', invoice.displayItemsTaxAmount);
    }
    // await printLeftRight('總計', '1,000元');
    await printLeftRight('總計', invoice.displayItemsTotalAmount);

    if (invoice.showTax) {
      await SunmiPrinter.hr();
      // await printLeftRight('總計', '1,000元');
      await printLeftRight('總計', invoice.displayItemsTotalAmount);
      // await SunmiPrinter.hr();
    }
  }

  static Future<void> printLeftRight(
    String lhs,
    String rhs, {
    bool reverse,
    SunmiSize textSize,
  }) async {
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '買方83193989',
          text: lhs ?? '',
          width: 6,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '賣方12345678',
          text: rhs ?? '',
          width: 6,
          align: SunmiAlign.right,
        ),
      ],
      textSize: textSize ?? SunmiSize.def,
      reverse: reverse ?? false,
    );
  }

  static Future<void> printHeader(String value, [String ch = '-']) async {
    final line = List.filled(6, ch).join();
    await SunmiPrinter.row(
      textSize: SunmiSize(30),
      cols: [
        SunmiCol(
          text: line,
          align: SunmiAlign.left,
          width: 3,
        ),
        SunmiCol(
          // text: '消費明細',
          text: value ?? '',
          align: SunmiAlign.center,
          width: 6,
        ),
        SunmiCol(
          text: line,
          align: SunmiAlign.right,
          width: 3,
        ),
      ],
    );
  }

  static Future<void> _printCenterReverse(String text,
      [bool reverse = true]) async {
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: text,
          width: 12,
          align: SunmiAlign.center,
        ),
      ],
      textSize: SunmiSize(28),
      reverse: reverse ?? true,
    );
  }

  static Future<void> printCenter(String text) async {
    await SunmiPrinter.text(
      text,
      styles: SunmiStyles(
        size: SunmiSize.def,
        align: SunmiAlign.center,
      ),
    );
  }

  static Future<void> printRight(String text) async {
    await SunmiPrinter.text(
      text,
      styles: SunmiStyles(
        size: SunmiSize.def,
        align: SunmiAlign.right,
      ),
    );
  }

  static Future<void> printText(String text) async {
    await SunmiPrinter.text(
      text,
      styles: SunmiStyles(
        size: SunmiSize.def,
      ),
    );
  }

  static Future<void> printLeftRightArgs(
    PrintArgs left,
    PrintArgs right, {
    SunmiSize size,
  }) async {
    await SunmiPrinter.row(
      textSize: size ?? SunmiSize.def,
      cols: [
        SunmiCol(
          text: left.text,
          width: 6,
          align: left.align ?? SunmiAlign.left,
        ),
        SunmiCol(
          text: right.text,
          width: 6,
          align: right.align ?? SunmiAlign.right,
        ),
      ],
    );
  }
}

class PrintArgs {
  final String text;
  final SunmiAlign align;
  final SunmiSize size;

  PrintArgs(
    this.text, {
    this.align,
    this.size,
  });
}
