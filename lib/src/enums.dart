/*
 * flutter_sunmi_printer
 * Created by Andrey U.
 * 
 * Copyright (c) 2020. All rights reserved.
 * See LICENSE for distribution and usage details.
 */

class SunmiAlign {
  final num value;
  const SunmiAlign._internal(this.value);
  static const left = SunmiAlign._internal(0); // 置左
  static const center = SunmiAlign._internal(1); // 置中
  static const right = SunmiAlign._internal(2); // 置右
}

class SunmiSize {
  final num value;
  const SunmiSize(this.value);
  const SunmiSize._internal(this.value);
  // factory SunmiSize.foo(final int value) => SunmiSize(value);
  // static SunmiSize create(final int value) => SunmiSize(value);
  static const xs = SunmiSize._internal(14);
  static const sm = SunmiSize._internal(18);
  static const md = SunmiSize._internal(24);
  static const lg = SunmiSize._internal(36);
  static const xl = SunmiSize._internal(42);
  static const xxl = SunmiSize._internal(48);
  static const x3l = SunmiSize._internal(64);
  static const x4l = SunmiSize._internal(72);
  static const x5l = SunmiSize._internal(96);
  static const x6l = SunmiSize._internal(144);
  static const def = SunmiSize._internal(26);
  static const title = SunmiSize._internal(30);
}

class BarCodeSymbology {
  final num value;
  const BarCodeSymbology._internal(this.value);
  static const UPC_A = BarCodeSymbology._internal(0);
  static const UPC_E = BarCodeSymbology._internal(1);
  static const JAN13 = BarCodeSymbology._internal(2);
  static const JAN8 = BarCodeSymbology._internal(3);
  static const CODE39 = BarCodeSymbology._internal(4);
  static const ITF = BarCodeSymbology._internal(5);
  static const CODABAR = BarCodeSymbology._internal(6);
  static const CODE93 = BarCodeSymbology._internal(7);
  static const CODE128 = BarCodeSymbology._internal(8);
}

class ErrorLevel {
  final num value;
  const ErrorLevel._internal(this.value);
  static const L = ErrorLevel._internal(-22); // ~~纠错级别L ( 7%)~~，// 更正為 qr code 的間隔
  static const M = ErrorLevel._internal(1); // 纠错级别M (15%)
  static const Q = ErrorLevel._internal(2); // 纠错级别Q (25%)
  static const H = ErrorLevel._internal(3); // 纠错级别H (30%)
}

class TextPosition {
  final num value;
  const TextPosition._internal(this.value);
  static const NONE = TextPosition._internal(0); // 不打印⽂字
  static const TOP = TextPosition._internal(1); // ⽂字在条码上⽅
  static const BOTTOM = TextPosition._internal(2); // ⽂字在条码下⽅
  static const BOTH = TextPosition._internal(3); // 条码上下⽅均打印
}

enum SunmiPrinterStatus {
  NoSunmiPrinter,
  CheckSunmiPrinter,
  FoundSunmiPrinter,
  LostSunmiPrinter,
  Max,
}
