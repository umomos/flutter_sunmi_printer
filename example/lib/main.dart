import 'dart:typed_data';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'constants.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _invoice = Invoice(
    storeName: '托比亞斯數位科技股份有限公司',
    printMark: 1,
    dateTime: DateTime.now(),
    invoiceNumber: 'AB12345678',
    randomNumber: '1234',
    seller: '83193989',
    buyer: '12656354',
    buyerName: '金財通商務科技服務股份有限公司',
    items: [
      Item(
        itemName: '口罩',
        quantity: 2,
        unitPrice: 210,
        taxRate: 0.05,
        taxType: BpscmTaxType.TX.value,
      ),
      Item(
        itemName: '牛奶',
        quantity: 1,
        unitPrice: 25,
        taxRate: 0.05,
        taxType: BpscmTaxType.TX.value,
      ),
      Item(
        itemName: '芒果',
        quantity: 1,
        unitPrice: 25,
        taxRate: 0.05,
        taxType: BpscmTaxType.Free.value,
      ),
    ],
  );

  void _print() async {
    // Test regular text
    SunmiPrinter.hr();
    SunmiPrinter.text(
      'Test Sunmi Printer',
      styles: SunmiStyles(align: SunmiAlign.center),
    );
    SunmiPrinter.hr();

    // Test align
    SunmiPrinter.text(
      'left',
      styles: SunmiStyles(bold: true, underline: true),
    );
    SunmiPrinter.text(
      'center',
      styles:
          SunmiStyles(bold: true, underline: true, align: SunmiAlign.center),
    );
    SunmiPrinter.text(
      'right',
      styles: SunmiStyles(bold: true, underline: true, align: SunmiAlign.right),
    );

    // Test text size
    SunmiPrinter.text('Extra small text',
        styles: SunmiStyles(size: SunmiSize.xs));
    SunmiPrinter.text('Medium text', styles: SunmiStyles(size: SunmiSize.md));
    SunmiPrinter.text('Large text', styles: SunmiStyles(size: SunmiSize.lg));
    SunmiPrinter.text('Extra large text',
        styles: SunmiStyles(size: SunmiSize.xl));

    // Test row
    SunmiPrinter.row(
      cols: [
        SunmiCol(text: 'col1', width: 4),
        SunmiCol(text: 'col2', width: 4, align: SunmiAlign.center),
        SunmiCol(text: 'col3', width: 4, align: SunmiAlign.right),
      ],
    );

    // Test image
    ByteData bytes = await rootBundle.load('assets/rabbit_black.jpg');
    final buffer = bytes.buffer;
    final imgData = base64.encode(Uint8List.view(buffer));
    SunmiPrinter.image(imgData);

    SunmiPrinter.emptyLines(3);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Test Sunmi Printer'),
        ),
        body: SingleChildScrollView(
          child: _main(),
        ),
      ),
    );
  }

  Widget _main() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 50),
        Center(
          child: ElevatedButton(
            onPressed: _print,
            child: const Text(
              'Print demo',
              style: TextStyle(fontSize: 20),
            ),
          ),
        ),
        Center(
          child: TextButton(
            onPressed: () {
              SunmiPrinter.feedPaper();
            },
            child: Text(
              'Feed paper',
              style: TextStyle(fontSize: 20),
            ),
          ),
        ),
        TextButton(
          onPressed: () {
            SunmiPrinter.setAlign(SunmiAlign.left);
            SunmiPrinter.printQrCode(
              data: kLeftQrCodeData,
              modulesize: 4,
              errorlevel: ErrorLevel.L,
            );
          },
          child: Text(
            'printQrCode',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            SunmiPrinter.text('4');
            SunmiPrinter.printBarCode(
              data: kBarCodeData,
              symbology: BarCodeSymbology.CODE39,
              height: 70,
              width: 1,
              textposition: TextPosition.NONE,
            );
          },
          child: Text(
            'printBarCode',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            SunmiPrinter.printDoubleQrCode(
              code1: kLeftQrCodeDataBase64.padRight(128, ' '),
              code2: kRightQrCodeDataBase64.padRight(128, ' '),
              modulesize: 210,
              errorlevel: ErrorLevel.L,
            );
          },
          child: Text(
            'printDoubleQrCode',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            SunmiPrinter.text('Extra large text',
                styles: SunmiStyles(size: SunmiSize.xl));
          },
          child: Text(
            'printText',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            // HACK:
            QrPainter(
              data: kLeftQrCodeData,
              // version: QrVersions.auto,
              version: 6,
              color: Colors.black,
              emptyColor: Colors.white,
              gapless: false,
            ).toImageData(200.0).then(
              (value) {
                final uri = Uri.dataFromBytes(value.buffer.asUint8List());
                SunmiPrinter.image(uri.data.contentText);
              },
            );
            // SunmiPrinter.text2('Extra large text',
            //     styles: SunmiStyles(size: SunmiSize.xl));
          },
          child: Text(
            'printText2',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            SunmiPrinter.print3Line();
          },
          child: Text(
            'print3Line',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            SunmiUtil.printDiscount(this._invoice);
          },
          child: Text(
            '列印折讓單',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            SunmiUtil.printInvoice(this._invoice);
            // SunmiUtil.printItems(value);
            // _invoice();
          },
          child: Text(
            'invoice',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            _printFoo();
          },
          child: Text(
            '日結單',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            _printReverse();
          },
          child: Text(
            '反白',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            _printFoo2();
          },
          child: Text(
            '當日商品銷售統計',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            getReceipt().then(SunmiUtil.printReceipt);
          },
          child: Text(
            '明細',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            getReceipt().then(SunmiUtil.printReceiptLite);
          },
          child: Text(
            '明細 Lite',
            style: TextStyle(fontSize: 20),
          ),
        ),
        TextButton(
          onPressed: () {
            getReceipt().then(SunmiUtil.printReceiptItems);
          },
          child: Text(
            '前結明細',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ],
    );
  }

  void _printReverse() {
    SunmiPrinter.text(
      'left',
      styles: SunmiStyles(
        bold: true,
        reverse: true,
      ),
    );
  }

  void _printFoo() async {
    final jsonString =
        await rootBundle.loadString('assets/print_statements.json');
    final data = PrintStatements.fromRawJson(jsonString);
    //PrintStatements
    await SunmiUtil.printReportsStatements(data);
  }

  void _printFoo2() async {
    final jsonString = await rootBundle.loadString('assets/print_sales.json');
    final data = PrintSales.fromRawJson(jsonString);
    //PrintStatements
    await SunmiUtil.printReportsSales(data);
  }

  Future<String> getJsonString(String filename) {
    // return rootBundle.loadString('assets/print_receipt.json');
    return rootBundle.loadString(filename);
  }

  Future<Receipt> getReceipt() {
    return getJsonString('assets/print_receipt.json').then((value) {
      return Receipt.fromRawJson(value);
    });
  }
}
