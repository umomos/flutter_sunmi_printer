# flutter_sunmi_printer

Plugin allows to print thermal receipts using Sunmi device with a built-in printer. Tested on Sunmi V2.

## Features

- `text`: print text with styles (bold, underline, align, font size)
- `row`: print a row containing up to 12 columns (total columns width must be equal to 12)
- `image`: print an image with alignment
- `hr`: print full width separator
- `emptyLines`: feed _n_ lines
- `boldOn`, `boldOff`
- `underlineOn`, `underlineOff`

## TODO

- Print QR codes
- Print Barcodes

## Getting Started

```dart
import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';

// Test regular text
SunmiPrinter.hr(); // prints a full width separator
SunmiPrinter.text(
    'Test Sunmi Printer',
    styles: SunmiStyles(align: SunmiAlign.center),
);
SunmiPrinter.hr();

// Test align
SunmiPrinter.text(
    'left',
    styles: SunmiStyles(bold: true, underline: true),
);
SunmiPrinter.text(
    'center',
    styles:
        SunmiStyles(bold: true, underline: true, align: SunmiAlign.center),
);
SunmiPrinter.text(
    'right',
    styles: SunmiStyles(bold: true, underline: true, align: SunmiAlign.right),
);

// Test text size
SunmiPrinter.text('Extra small text',
    styles: SunmiStyles(size: SunmiSize.xs));
SunmiPrinter.text('Medium text', styles: SunmiStyles(size: SunmiSize.md));
SunmiPrinter.text('Large text', styles: SunmiStyles(size: SunmiSize.lg));
SunmiPrinter.text('Extra large text',
    styles: SunmiStyles(size: SunmiSize.xl));

// Test row
SunmiPrinter.row(
    cols: [
        SunmiCol(text: 'col1', width: 4),
        SunmiCol(text: 'col2', width: 4, align: SunmiAlign.center),
        SunmiCol(text: 'col3', width: 4, align: SunmiAlign.right),
    ],
);

// Test image
ByteData bytes = await rootBundle.load('assets/rabbit_black.jpg');
final buffer = bytes.buffer;
final imgData = base64.encode(Uint8List.view(buffer));
SunmiPrinter.image(imgData);

SunmiPrinter.emptyLines(3);
```

## [测试发现商米机器的打印竟然可以使用桑达的打印驱动](https://gitlab.infra.miyatech.com/pengguangpu/miya-hardware/commit/eb5a96935bcb91cb8cf474611d955654899e2ddf)

### [深圳桑达商用机器有限公司](http://www.sedsy.com/)

## aa

另外，印表機可能都會有這個指令 reverse
ref - https://pub.dev/packages/esc_pos_printer
printer.text('Reverse text', styles: PosStyles(reverse: true));

* [android 控制POS機圖文列印（一）](https://www.itread01.com/p/98285.html)

* [android 控制POS機圖文列印（二）](https://www.itread01.com/p/98281.html)

* [印表機命令](https://www.itread01.com/content/1549478344.html)

### [ESC/POS命令集摘](https://topic.alibabacloud.com/tc/a/escpos-command-set-extraction_8_8_31924583.html)

### [esc_pos_utils](https://pub.dev/packages/esc_pos_utils)

### [esc_pos_printer](https://pub.dev/packages/esc_pos_printer)

## [Action](https://stackoverflow.com/a/51366801)

```dart
/// As a method parameter.
void takesACallback(void Function(int, String) callback) {}

/// As a type parameter.
final mapOfFunctions = <int, String Function(bool)>{};

/// Named with a typedef.
typedef MyCallback = void Function(int);

/// Using the typedef
void myMethod(MyCallback myCallback) {}
```

## 開發 package

* [撰写双端平台代码（插件编写实现）](https://flutter.cn/docs/development/platform-integration/platform-channels?tab=android-channel-java-tab)

* [Flutter Packages 的开发和提交](https://flutter.cn/docs/development/packages-and-plugins/developing-packages)

* [12.2 插件开发：平台通道简介](https://book.flutterchina.club/chapter12/platform-channel.html)

## zxing package

- [zxing-android-embedded](https://github.com/journeyapps/zxing-android-embedded)

- Config for SDK 24+
  - implementation 'com.journeyapps:zxing-android-embedded:4.3.0'
- Older SDK versions
  - Option 1. Downgrade zxing:core to 3.3.0
    - implementation('com.journeyapps:zxing-android-embedded:4.3.0') { transitive = false }
    - implementation 'com.google.zxing:core:3.3.0'
- [zxing](https://github.com/zxing/zxing)
