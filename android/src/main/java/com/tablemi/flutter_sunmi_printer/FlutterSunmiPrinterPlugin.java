package com.tablemi.flutter_sunmi_printer;

import com.tablemi.flutter_sunmi_printer.utils.SunmiPrintHelper;

import java.util.HashMap;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

public class FlutterSunmiPrinterPlugin implements FlutterPlugin, MethodCallHandler {
    private MethodChannel channel;
    private static FlutterSunmiPrinterModule flutterSunmiPrinterModule;

    private static final String RESET = "reset";
    private static final String START_PRINT = "startPrint";
    private static final String STOP_PRINT = "stopPrint";
    private static final String IS_PRINTING = "isPrinting";
    private static final String BOLD_ON = "boldOn";
    private static final String BOLD_OFF = "boldOff";
    private static final String UNDERLINE_ON = "underlineOn";
    private static final String UNDERLINE_OFF = "underlineOff";
    private static final String EMPTY_LINES = "emptyLines";
    private static final String PRINT_TEXT = "printText";
    private static final String PRINT_ROW = "printRow";
    private static final String PRINT_IMAGE = "printImage";
    private static final String SEND_RAW_DATA = "sendRawData";
    private static final String FEED_PAPER = "feedPaper";
    private static final String PRINT_ORIGIN_TEXT = "printOriginalText";
    private static final String PRINT_TEXT_2 = "printText2";
    private static final String PRINT_TABLE_ITEM = "printTableItem";
    private static final String SET_FONT_SIZE = "setFontSize";
    private static final String LINE_WRAP = "lineWrap";
    private static final String PRINT_TABLE = "printTable";
    private static final String PRINT_QR_CODE = "printQrCode";
    private static final String PRINT_BAR_CODE = "printBarCode";
    private static final String SET_ALIGN = "setAlign";
    private static final String PRINT_3_LINE = "print3Line";
    private static final String PRINT_DOUBLE_QR_CODE = "printDoubleQrCode";
    private static final String SET_PRINTER_STYLE = "setPrinterStyle";
    private static final String REVERSE_ON = "reverseOn";
    private static final String REVERSE_OFF = "reverseOff";
    private static final String CUT_PAPER = "cutPaper";
    private static final String SUNMI_PRINTER_STATUS = "sunmiPrinterStatus";

    private final Map<String, BiConsumer<MethodCall, Result>> actions = new HashMap<>();

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_sunmi_printer");
        channel.setMethodCallHandler(this);
        flutterSunmiPrinterModule = new FlutterSunmiPrinterModule();
        flutterSunmiPrinterModule.initAidl(flutterPluginBinding.getApplicationContext());

        actions.put(FEED_PAPER, (call, result) -> {
            SunmiPrintHelper.getInstance().feedPaper();
            result.success(null);
        });
        actions.put(RESET, (call, result) -> {
            flutterSunmiPrinterModule.reset();
            result.success(null);
        });
        actions.put(START_PRINT, (call, result) -> {
            flutterSunmiPrinterModule.startPrint();
            result.success(null);
        });
        actions.put(STOP_PRINT, (call, result) -> {
            flutterSunmiPrinterModule.stopPrint();
            result.success(null);
        });
        actions.put(IS_PRINTING, (call, result) -> {
            result.success(flutterSunmiPrinterModule.isPrinting());
        });
        actions.put(REVERSE_ON, (call, result) -> {
            flutterSunmiPrinterModule.reverseOn();
            result.success(null);
        });
        actions.put(REVERSE_OFF, (call, result) -> {
            flutterSunmiPrinterModule.reverseOff();
            result.success(null);
        });
        actions.put(BOLD_ON, (call, result) -> {
            flutterSunmiPrinterModule.boldOn();
            result.success(null);
        });
        actions.put(BOLD_OFF, (call, result) -> {
            flutterSunmiPrinterModule.boldOff();
            result.success(null);
        });
        actions.put(UNDERLINE_ON, (call, result) -> {
            flutterSunmiPrinterModule.underlineOn();
            result.success(null);
        });
        actions.put(UNDERLINE_OFF, (call, result) -> {
            flutterSunmiPrinterModule.underlineOff();
            result.success(null);
        });
        actions.put(EMPTY_LINES, (call, result) -> {
            int n = call.argument("n");
            flutterSunmiPrinterModule.emptyLines(n);
            result.success(null);
        });
        actions.put(PRINT_TEXT, (call, result) -> {
            String text = call.argument("text");
            int align = call.argument("align");
            boolean bold = call.argument("bold");
            boolean underline = call.argument("underline");
            int linesAfter = call.argument("linesAfter");
            int size = call.argument("size");
            boolean reverse = call.argument("reverse");
            flutterSunmiPrinterModule.text(text, align, bold, underline, size, linesAfter, reverse);
            result.success(null);
        });
        actions.put(PRINT_ROW, (call, result) -> {
            String cols = call.argument("cols");
            boolean bold = call.argument("bold");
            boolean underline = call.argument("underline");
            int textSize = call.argument("textSize");
            int linesAfter = call.argument("linesAfter");
            boolean reverse = call.argument("reverse");
            flutterSunmiPrinterModule.row(cols, bold, underline, textSize, linesAfter, reverse);
            result.success(null);
        });
        actions.put(PRINT_IMAGE, (call, result) -> {
            String base64 = call.argument("base64");
            int align = call.argument("align");
            flutterSunmiPrinterModule.printImage(base64, align);
            result.success(null);
        });
        actions.put(SEND_RAW_DATA, (call, result) -> {
            byte[] data = call.argument("data");
            SunmiPrintHelper.getInstance().sendRawData(data);
            result.success(null);
        });
        actions.put(PRINT_ORIGIN_TEXT, (call, result) -> {
            String text = call.argument("text");
            SunmiPrintHelper.getInstance().printOriginalText(text);
            result.success(null);
        });
        actions.put(PRINT_TEXT_2, (call, result) -> {
            String content = call.argument("content");
            int size = call.argument("size");
            boolean isBold = call.argument("isBold");
            boolean isUnderLine = call.argument("isUnderLine");
            SunmiPrintHelper.getInstance().printText(content + "\n", size, isBold, isUnderLine, null);
            result.success(null);
        });
        actions.put(PRINT_TABLE_ITEM, (call, result) -> {
            String[] txts = call.argument("txts");
            int[] width = call.argument("width");
            int[] align = call.argument("align");
            SunmiPrintHelper.getInstance().printTableItem(txts, width, align);
            result.success(null);
        });
        actions.put(SET_FONT_SIZE, (call, result) -> {
            int size = call.argument("size");
            SunmiPrintHelper.getInstance().setFontSize(size);
            result.success(null);
        });
        actions.put(LINE_WRAP, (call, result) -> {
            int n = call.argument("n");
            SunmiPrintHelper.getInstance().lineWrap(n);
            result.success(null);
        });
        actions.put(PRINT_TABLE, (call, result) -> {
            String[] txts = call.argument("txts");
            int[] width = call.argument("width");
            int[] align = call.argument("align");
            SunmiPrintHelper.getInstance().printTable(txts, width, align);
            result.success(null);
        });
        actions.put(PRINT_QR_CODE, (call, result) -> {
            String data = call.argument("data");
            int modulesize = call.argument("modulesize");
            int errorlevel = call.argument("errorlevel");
            SunmiPrintHelper.getInstance().printQr(data, modulesize, errorlevel);
            result.success(null);
        });
        actions.put(PRINT_BAR_CODE, (call, result) -> {
            String data = call.argument("data");
            int symbology = call.argument("symbology");
            int height = call.argument("height");
            int width = call.argument("width");
            int textposition = call.argument("textposition");
            SunmiPrintHelper.getInstance().printBarCode(data, symbology, height, width, textposition);
            result.success(null);
        });
        actions.put(SET_ALIGN, (call, result) -> {
            int align = call.argument("align");
            SunmiPrintHelper.getInstance().setAlign(align);
            result.success(null);
        });
        actions.put(PRINT_3_LINE, (call, result) -> {
            SunmiPrintHelper.getInstance().print3Line();
            result.success(null);
        });
        actions.put(PRINT_DOUBLE_QR_CODE, (call, result) -> {
            String code1 = call.argument("code1");
            String code2 = call.argument("code2");
            int modulesize = call.argument("modulesize");
            int errorlevel = call.argument("errorlevel");
            SunmiPrintHelper.getInstance().printDoubleQRCode(code1, code2, modulesize, errorlevel);
            result.success(null);
        });
        actions.put(SET_PRINTER_STYLE, (call, result) -> {
            int key = call.argument("key");
            int val = call.argument("val");
            SunmiPrintHelper.getInstance().setPrinterStyle(key, val);
            result.success(null);
        });
        actions.put(CUT_PAPER, (call, result) -> {
            SunmiPrintHelper.getInstance().cutpaper();
            result.success(null);
        });
        actions.put(SUNMI_PRINTER_STATUS, (call, result) -> {
            result.success(SunmiPrintHelper.getInstance().sunmiPrinter);
        });
    }

    // This static function is optional and equivalent to onAttachedToEngine. It
    // supports the old pre-Flutter-1.12 Android projects. You are encouraged to
    // continue supporting plugin registration via this function while apps migrate
    // to use the new Android APIs post-flutter-1.12 via
    // https://flutter.dev/go/android-project-migration.
    //
    // It is encouraged to share logic between onAttachedToEngine and registerWith
    // to keep them functionally equivalent. Only one of onAttachedToEngine or
    // registerWith will be called depending on the user's project.
    // onAttachedToEngine or registerWith must both be defined in the same class.
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_sunmi_printer");
        channel.setMethodCallHandler(new FlutterSunmiPrinterPlugin());
        flutterSunmiPrinterModule = new FlutterSunmiPrinterModule();
        flutterSunmiPrinterModule.initAidl(registrar.context());
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (actions.containsKey(call.method)) {
            try {
                actions.get(call.method).accept(call, result);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        } else {
            result.notImplemented();
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        flutterSunmiPrinterModule.deinit(binding.getApplicationContext());
        channel.setMethodCallHandler(null);
    }
}
