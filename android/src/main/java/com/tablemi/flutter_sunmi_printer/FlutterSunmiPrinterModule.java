package com.tablemi.flutter_sunmi_printer;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.RemoteException;
import android.util.Log;

import com.sunmi.peripheral.printer.InnerResultCallback;
//import com.tablemi.flutter_sunmi_printer.utils.AidlUtil;
import com.tablemi.flutter_sunmi_printer.utils.Base64Utils;
import com.tablemi.flutter_sunmi_printer.utils.BitmapUtil;
import com.tablemi.flutter_sunmi_printer.utils.ESCUtil;
import com.tablemi.flutter_sunmi_printer.utils.SunmiPrintHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Module 用於初始，其餘在 SunmiPrinterModule 使用較多
 */
public class FlutterSunmiPrinterModule {

    private static boolean isPrinting = false;
    public static int DEFAULT_FONT_SIZE = 24;

    public void initAidl(Context context) {
//    AidlUtil.getInstance().connectPrinterService(context);
//    AidlUtil.getInstance().initPrinter();
        SunmiPrintHelper.getInstance().initSunmiPrinterService(context);
    }

    public void deinit(Context context) {
        SunmiPrintHelper.getInstance().deInitSunmiPrinterService(context);
    }

    public void reset() {
//    SunmiPrintHelper.getInstance().sunmiPrinterService.
//    AidlUtil.getInstance().initPrinter();
    }

    public void startPrint() {
        isPrinting = true;
    }

    public void stopPrint() {
        isPrinting = false;
    }

    public boolean isPrinting() {
        return isPrinting;
    }

    public void reverseOn() {
        SunmiPrintHelper.getInstance().sendRawData(ESCUtil.reverseOn());
    }

    public void reverseOff() {
        SunmiPrintHelper.getInstance().sendRawData(ESCUtil.reverseOff());
    }

    public void boldOn() {
        SunmiPrintHelper.getInstance().sendRawData(ESCUtil.boldOn());
    }

    public void boldOff() {
        SunmiPrintHelper.getInstance().sendRawData(ESCUtil.boldOff());
    }

    public void underlineOn() {
        SunmiPrintHelper.getInstance().sendRawData(ESCUtil.underlineWithOneDotWidthOn());
    }

    public void underlineOff() {
        SunmiPrintHelper.getInstance().sendRawData(ESCUtil.underlineOff());
    }

    public void emptyLines(int n) {
        SunmiPrintHelper.getInstance().lineWrap(n);
    }

    public void setFontSize(int size) {
        SunmiPrintHelper.getInstance().setFontSize(size);
    }

    public void text(String text, int align, boolean bold, boolean underline, int size, int linesAfter, boolean reverse) {
        // Set styles
        if (bold) {
            boldOn();
        }
        if (underline) {
            underlineOn();
        }
        if (reverse) {
            reverseOn();
        }

        // Print text
        setFontSize(size);
        SunmiPrintHelper.getInstance().printTableItem(new String[]{text}, new int[]{32}, new int[]{align});
        if (linesAfter > 0) {
            emptyLines(linesAfter);
        }
        setFontSize(DEFAULT_FONT_SIZE);

        // Reset styles
        if (reverse) {
            reverseOff();
        }
        if (bold) {
            boldOff();
        }
        if (underline) {
            underlineOff();
        }
    }

    public void row(String colsStr, boolean bold, boolean underline, int textSize, int linesAfter, boolean reverse) {
        try {
            // Set styles
            if (bold) {
                boldOn();
            }
            if (underline) {
                underlineOn();
            }
            if (reverse) {
                reverseOn();
            }

            // Prepare row data
            JSONArray cols = new JSONArray(colsStr);
            String[] colsText = new String[cols.length()];
            int[] colsWidth = new int[cols.length()];
            int[] colsAlign = new int[cols.length()];
            for (int i = 0; i < cols.length(); i++) {
                JSONObject col = cols.getJSONObject(i);
                String text = col.getString("text");
                int width = col.getInt("width");
                int align = col.getInt("align");
                colsText[i] = text;
                colsWidth[i] = width;
                colsAlign[i] = align;
            }

            // Print row
            setFontSize(textSize);
            SunmiPrintHelper.getInstance().printTableItem(colsText, colsWidth, colsAlign);
            if (linesAfter > 0) {
                emptyLines(linesAfter);
            }
            setFontSize(DEFAULT_FONT_SIZE);

            // Reset styles
            if (reverse) {
                reverseOff();
            }
            if (bold) {
                boldOff();
            }
            if (underline) {
                underlineOff();
            }
        } catch (Exception err) {
            Log.d("SunmiPrinter", err.getMessage());
        }
    }

    public void printImage(String base64, int align) {
        byte[] bytes = Base64Utils.decode(base64);
        for (int i = 0; i < bytes.length; ++i) {
            // ajust data
            if (bytes[i] < 0) {
                bytes[i] += 256;
            }
        }
        SunmiPrintHelper.getInstance().printBitmap(BitmapUtil.convertToThumb(bytes, 280), align);
        SunmiPrintHelper.getInstance().lineWrap(1);
    }

    public void cutPaper() {
        SunmiPrintHelper.getInstance().cutpaper();
    }
}
